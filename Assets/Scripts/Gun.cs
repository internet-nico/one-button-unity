﻿using UnityEngine;
using System.Collections;

public class Gun : MonoBehaviour { 

	public static event ExpireAction OnGunExpired;
	public delegate void ExpireAction(Gun gun);

	public bool active = true;
	public float lifeSpan = 5.0f;
	public float spawnRate = 0.1f;

	private GameManager gameManager;

	// Use this for initialization
	public virtual void Start () {
//		Restart ();
	}

	public virtual void Stop() {
		StopAllCoroutines ();

		// Pool all active bullets
		foreach (Transform t in transform) {
			if (t.gameObject.name == "Bullet") {
				// Remove from container
				t.gameObject.transform.parent = null;
				// Put it back
				ObjectPool.instance.PoolObject (t.gameObject);
			}
		}
		
		// Dispatch event
		if (OnGunExpired != null)
			OnGunExpired (this);
	}

	void OnDestroy() {
		StopAllCoroutines ();
	}

	void Awake() {
		// Get Components
		GameObject game = GameObject.Find("Game");
		gameManager = game.GetComponent<GameManager>();
	}

	// Reset vars
	public virtual void Reset(Vector3 _position) {
		// Set position
		gameObject.transform.position = _position;
	}

	// Restart all coroutines
	public virtual void Restart() {
		StopAllCoroutines ();
		// Start
		StartCoroutine (GunRoutine ());
		StartCoroutine (MoveIt ());
	}

	protected void StartLifespan() {
		StartCoroutine (WaitForLifespan ());
	}

	public virtual void Fire(Bullet bullet) {
		// Must override
	}

	IEnumerator GunRoutine() {

		while (true) {
			if (active && ! gameManager.GetIsPaused ()) {
				// Get a bullet (only if available)
				var bulletGameObject = ObjectPool.instance.GetObjectForType("Bullet", true);

				if (bulletGameObject != null) {
					// Get the script
					var bullet = bulletGameObject.GetComponent<Bullet> ();
					// Fire.
					Fire (bullet);
				}
			}

			yield return new WaitForSeconds (spawnRate);
		}
	}

	IEnumerator MoveIt() {
		
		while (true) {
			if (active && ! gameManager.GetIsPaused ()) {
				Vector3 position = gameObject.transform.position;
				position.x -= 1;
				
				gameObject.transform.position = position;
			}

			yield return null;
		}
	}

	protected IEnumerator WaitForLifespan() {

		// Just wait
		yield return new WaitForSeconds(lifeSpan);

		// Stop
		Stop ();
	}
}
