﻿using UnityEngine;
using System.Collections;

public class Spawner : MonoBehaviour {

	public bool active = true;
	public float spawnRate = 5f;

	private GameManager gameManager;

	protected string[] prefabNames = new string[]{ };

	// Use this for initialization
	protected virtual void Start () {
		// Get Components
		GameObject game = GameObject.Find("Game");
		gameManager = game.GetComponent<GameManager>();
	}

	protected virtual void Restart() {
		StopAllCoroutines ();

		// Start
		StartCoroutine (CreateRoutine ());
	}

	protected virtual void RestartSpawnedObject(GameObject prefabGameObject) {
		// Must override
	}

	IEnumerator CreateRoutine() {

		while (true) {
			if (gameManager.GetIsPaused ())
				yield return null;

			if (active) {
				// Get a prefab (only if available)
				var prefabIndex = Random.Range(0, prefabNames.Length);
				var prefabGameObject = ObjectPool.instance.GetObjectForType(prefabNames[prefabIndex], true);

				if (prefabGameObject != null) {
					// Add it to container
					prefabGameObject.transform.parent = gameObject.transform;

					// Subclasses will override 
					RestartSpawnedObject (prefabGameObject);
				}
			}

			yield return new WaitForSeconds (spawnRate);
		}
	}

	protected void DespawnGameObject(GameObject prefabGameObject) {
		// Remove from container
		prefabGameObject.transform.parent = null;
		// Put it back
		ObjectPool.instance.PoolObject (prefabGameObject);
	}
}
