﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SpacebarText : MonoBehaviour {

	public float waitTime = 3f;

	private Text theText;

	// Use this for initialization
	void Start () {
		// Get components
		theText = GetComponent<Text>();

		// Start disabled
		theText.enabled = false;

		// Start animations in a few seconds
		Invoke("StartAnimations", waitTime);
	}

	void StartAnimations() {
		// Start Blink
		StartCoroutine (HelperMethods.Blink (theText, 99f, 0.42f));
	}
}
