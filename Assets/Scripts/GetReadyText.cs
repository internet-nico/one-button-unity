﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GetReadyText : MonoBehaviour {

	private Text theText;
	private float waitTime = 0f;

	// Use this for initialization
	void Start () {
		// Get components
		theText = GetComponent<Text>();

		// Start disabled
		theText.enabled = false;

		// Start animations in a few seconds
		Invoke("StartAnimations", waitTime);
	}
	
	void StartAnimations() {
		// Start Blink
		StartCoroutine (HelperMethods.Blink (theText, 0.07f, 0.42f));
	}
}
