﻿using UnityEngine;
using System.Collections;

public class SpiralGun : Gun {

	public float rotateRate = 10.0f;
	public float force = 500.0f;
	public float bulletLife = 3.0f;

	private float degrees = 0.0f;

	// Use this for initialization
	public override void Start () {
		base.Start ();

		// Listen for events
		Bullet.OnBulletExpired += OnBulletExpiredHandler;
	}

	void OnDestroy() {
		StopAllCoroutines ();

		// Remove event listeners
		Bullet.OnBulletExpired -= OnBulletExpiredHandler;
	}

	void OnTriggerEnter2D(Collider2D other) {
		if (other.name == "Death") {
			base.Stop ();
		}
	}

	public override void Reset(Vector3 _position) {
		base.Reset (_position);

		// Random spiral vars
		spawnRate = Random.Range (0.25f, 0.5f);
		rotateRate = Random.Range (15f, 30f);
		degrees = Random.Range (0f, 360f);
		force = Random.Range (15000f, 30000f);
	}

	public override void Fire(Bullet bullet) {
		// Add it to container
		bullet.gameObject.transform.parent = gameObject.transform;
		
		// Calc vector along circle
		var rad = degrees * Mathf.Deg2Rad;
		var v2 = new Vector2(Mathf.Cos(rad)*force, Mathf.Sin(rad)*force);
		
		// Reset position to identity
		bullet.Reset (new Vector3(), bulletLife, GetInstanceID());
		
		// Fire with force
		bullet.FireForceWithVector(v2);

		// Start lifespan
		bullet.StartLifespan();
		
		// Update data for next time
		degrees += rotateRate;

		// Dispatch event
		EventManager.instance.QueueEvent (new BulletFiredEvent ());
	}

	void OnBulletExpiredHandler(Bullet bullet, int gunId) {
		// Check if the bullet's "gunId" matches this gun
		if (gunId == GetInstanceID ()) {
			// Remove from container
			bullet.gameObject.transform.parent = null;
			// Put it back
			ObjectPool.instance.PoolObject (bullet.gameObject);
		}
	}
}
