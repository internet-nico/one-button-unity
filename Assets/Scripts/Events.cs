﻿using UnityEngine;
using UnityEngine.Events;

public class DeathEvent : IEvent
{
	public DeathEvent()
	{

	}

	string IEvent.GetName()
	{
		return this.GetType().ToString();
	}

	object IEvent.GetData()
	{
		return "DeathEvent Data goes here!";
	}
}

public class RestartEvent : IEvent
{
	public RestartEvent()
	{

	}

	string IEvent.GetName()
	{
		return this.GetType().ToString();
	}

	object IEvent.GetData()
	{
		return "RestartEvent Data goes here!";
	}
}

public class BulletExpiredEvent : IEvent
{
	public int gunId;

	private GameObject bullet;

	public BulletExpiredEvent(GameObject bullet)
	{
		this.bullet = bullet;
	}

	string IEvent.GetName()
	{
		return this.GetType().ToString();
	}

	object IEvent.GetData()
	{
		return bullet;
	}
}

public class BulletFiredEvent : IEvent
{
	public BulletFiredEvent()
	{

	}

	string IEvent.GetName()
	{
		return this.GetType().ToString();
	}

	object IEvent.GetData()
	{
		return "BulletFiredEvent Data goes here!";
	}
}