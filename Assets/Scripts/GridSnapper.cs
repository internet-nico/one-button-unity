﻿using UnityEngine;
using System.Collections;

public class GridSnapper : MonoBehaviour {

	public GameObject child;
	public int gridSize = 4;
	public float offsetX = 2.0f;
	public float offsetY = 2.0f;

	void LateUpdate() {
		Vector3 newPos = Vector3.zero;
//		newPos.x = -Mathf.Abs((gameObject.transform.position.x % gridSize)) + offsetX;
//		newPos.y = -Mathf.Abs((gameObject.transform.position.y % gridSize)) + offsetY;
		if (gameObject.transform.position.y > 0) {
			newPos.x = -(gameObject.transform.position.x % gridSize) - offsetX;
			newPos.y = -(gameObject.transform.position.y % gridSize) - offsetY;
		} else {
			newPos.x = -(gameObject.transform.position.x % gridSize) + offsetX;
			newPos.y = -(gameObject.transform.position.y % gridSize) + offsetY;
		}

//		if (newPos.y < 0.02f || newPos.y > 3.98f) {
//			newPos.y = 0.0f;
//		}

//		var worldPos = child.transform.TransformPoint (Vector3.zero);
//		Debug.Log ("local: " + child.transform.position + ", world: " + worldPos);
//		worldPos.x = -(worldPos.x % gridSize) + offsetX;
//		worldPos.y = -(worldPos.y % gridSize) + offsetY;
//
//		var localPos = child.transform.InverseTransformPoint (worldPos);

//		Debug.Log (diff);

//		Debug.Log (newPos);

		child.transform.localPosition = newPos;
	}
}
