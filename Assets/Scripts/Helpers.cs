﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public static class HelperMethods
{
	public static List<GameObject> GetChildrenOf(this GameObject go)
	{
		List<GameObject> children = new List<GameObject>();
		foreach (Transform tran in go.transform)
		{
			children.Add(tran.gameObject);
		}
		return children;
	}

	public static IEnumerator FadeIn(SpriteRenderer spriteRenderer, float totalTime = 2.0f) {
		// Animate
		float t = 0.0f;
		while (t <= 1.0f) {
			t += Time.deltaTime / totalTime;

			// Set color alpha
			var color = spriteRenderer.color;
			color.a = Mathf.Lerp (0.0f, 1.0f, Mathf.SmoothStep (0.0f, 1.0f, t));
			spriteRenderer.color = color;

			yield return null;
		}

		// Finish, set color alpha to 1
		var colorEnd = spriteRenderer.color;
		colorEnd.a = 1.0f;
		spriteRenderer.color = colorEnd;
	}

	public static IEnumerator FadeIn(Text text, float totalTime = 2.0f) {
		// Animate
		float t = 0.0f;
		while (t <= 1.0f) {
			t += Time.deltaTime / totalTime;

			// Set alpha
			var color = text.color;
			color.a = Mathf.Lerp (0.0f, 1.0f, Mathf.SmoothStep (0.0f, 1.0f, t));
			text.color = color;

			yield return null;
		}

		// Finish, set alpha to 1
		var colorEnd = text.color;
		colorEnd.a = 1.0f;
		text.color = colorEnd;
	}

	public static IEnumerator Blink(SpriteRenderer spriteRenderer, float duration, float blinkTime) {
		while (duration > 0f) {
			duration -= Time.deltaTime;

			// toggle renderer
			spriteRenderer.enabled = !spriteRenderer.enabled;

			// wait for a bit
			yield return new WaitForSeconds (blinkTime);
		}

		// Finish, make sure renderer is enabled when we exit
		spriteRenderer.enabled = true;
	}

	public static IEnumerator Blink(Text theText, float duration, float blinkTime) {
		while (duration > 0f) {
			duration -= Time.deltaTime;
			// toggle renderer
			theText.enabled = !theText.enabled;

			// wait for a bit
			yield return new WaitForSeconds (blinkTime);
		}

		// Finish, make sure renderer is enabled when we exit
		theText.enabled = false;
	}
}