﻿using UnityEngine;
using System.Collections;

public class RandomRightsideGun : Gun {

	// Use this for initialization
	public override void Start () {
		base.Start ();

		// Listen for events
		Bullet.OnBulletExpired += OnBulletExpiredHandler;

		// Start
		base.Restart();
	}

	void OnDestroy() {
		StopAllCoroutines ();

		// Remove event listeners
		Bullet.OnBulletExpired -= OnBulletExpiredHandler;
	}

	public override void Fire(Bullet bullet) {
		// Add it to container
		bullet.gameObject.transform.parent = gameObject.transform;
		
		// Calc positions
		Vector3 pos = new Vector3(160f, Random.Range(-66f, 66f), 0);
		Vector3 endPos = pos;
		endPos.x -= 420;
		float speed = Random.Range (30f, 60f);
		
		// Reset position
		bullet.Reset (pos, 5.0f, GetInstanceID());
		
		// Fire linearly to end position
		bullet.FireLinearToPosition(endPos, speed);
		
		// Change spawnRate for next bullet
		spawnRate = Random.Range(0.1f, 1f);
	}

	void OnBulletExpiredHandler(Bullet bullet, int gunId) {
		// Check if the bullet's "gunId" matches this gun
		if (gunId == GetInstanceID ()) {
			// Remove from container
			bullet.gameObject.transform.parent = null;
			// Put it back
			ObjectPool.instance.PoolObject (bullet.gameObject);
		}
	}
}
