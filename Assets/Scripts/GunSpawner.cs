﻿using UnityEngine;
using System.Collections;

public class GunSpawner : Spawner {

	// Use this for initialization
	protected override void Start () {
		base.Start ();

		// Mark prefabs to spawn
		prefabNames = new string[]{ "SpiralGun" };

		// Listen for events
		Gun.OnGunExpired += OnGunExpiredHandler;

		// Start soon
		Invoke("Restart", Random.Range(8f, 15f));
	}

	void OnDestroy() {
		// Stop listening
		Gun.OnGunExpired -= OnGunExpiredHandler;
	}

	protected override void RestartSpawnedObject(GameObject prefabGameObject) {
		// Get the script
		var gun = prefabGameObject.GetComponent<Gun> ();

		if (gun.gameObject.name == "SpiralGun") {
			if (Random.value > 0.5)
				// Start on ground
				gun.Reset (new Vector3 (160f, -66f));
			else
				// Start on ceiling
				gun.Reset (new Vector3 (160f, 66f));
		} else {
			// Start Randomly
			gun.Reset (new Vector3(160f, Random.Range(-60, 60)));
		}

		// Start gun coroutines
		gun.Restart ();
	}

	void OnGunExpiredHandler(Gun gun) {
		// Despawn this gun
		base.DespawnGameObject (gun.gameObject);
	}
}
