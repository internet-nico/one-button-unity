﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class YouDiedText : MonoBehaviour {

//	public GameObject mainCamera;

	private Text theText;
	private Animator textAnimator;
	private float lastA = 0f;

	// Use this for initialization
	void Start () {
		// Get components
		theText = GetComponent<Text> ();
		textAnimator = GetComponent<Animator> ();

		// Start disabled
		theText.enabled = false;
	}

	public void StartAnimations() {
		// Start movement
		theText.enabled = true;
		// Trigger animation
		textAnimator.SetTrigger ("PlayerDied");
	}

	void LateUpdate() {
		Color theColor = theText.color;

		if (!Mathf.Approximately(theColor.a, lastA)) {
			float a;
			if (!Mathf.Approximately (theColor.a, 1f))
				a = theColor.a - ((theColor.a * 100f) % 20f) / 100f;
			else 
				a = 1f;

			theColor.a = a;
			theText.color = theColor;
		}

		lastA = theColor.a;
	}
}
