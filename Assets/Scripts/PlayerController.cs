﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {

	public float speed = 0f;
	public float jumpForce;

	private Rigidbody2D rb2d;
	private Animator animator;
	private float movement = 0f;
	private float maxVerticalSpeed = 100.0f;

	// Use this for initialization
	void Start () {
		// Get components
		rb2d = GetComponent<Rigidbody2D> ();
		animator = GetComponent<Animator> ();
	}
	
	// Update is called once per frame
//	void Update () {
//	
//	}

	public void Restart() {
		rb2d.isKinematic = true;
		animator.enabled = true;
		animator.SetTrigger("RestartLevel");
	}

	public void StartAnimationCompleted() {
		
	}

	public void EnablePhysics() {
		animator.Play ("Default");
		animator.enabled = false;
		rb2d.isKinematic = false;
	}

	void OnTriggerEnter2D(Collider2D other) {
		if (other.name == "Death"
			|| other.name == "Bullet"
			|| other.name == "Bullet(Clone)") {

			// Dispatch event on death
			EventManager.instance.TriggerEvent (new DeathEvent ());
		}
	}

	void OnCollisionEnter2D(Collision2D other) {
		if (other.gameObject.name == "Ceiling"
		   || other.gameObject.name == "Ground") {

			// Dispatch event on death
			EventManager.instance.TriggerEvent (new DeathEvent ());
		}
	}

	void FixedUpdate () {
//		movement = Input.GetAxis ("Horizontal");
//		rb2d.velocity = new Vector2 (movement * speed, rb2d.velocity.y);

		//
		// THE BUTTON
		//
		if ((Input.GetKey (KeyCode.Space) || Input.GetMouseButton(0)))
		{
			// Add force
			rb2d.AddForce (new Vector2 (rb2d.velocity.x, jumpForce));
		}

		// Clamp vertical velocity
		if (rb2d.velocity.y > maxVerticalSpeed) {
			rb2d.velocity = Vector3.ClampMagnitude (rb2d.velocity, maxVerticalSpeed);
		}
	}
}
