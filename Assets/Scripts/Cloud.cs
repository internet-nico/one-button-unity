﻿using UnityEngine;
using System.Collections;

public class Cloud : MonoBehaviour {

	public static event ExpireAction OnCloudExpired;
	public delegate void ExpireAction(Cloud cloud);

	private float totalTime = 10.0f;
	private float startX;
	private float endX;

	private GameManager gameManager;

	// Use this for initialization
	//	void Start () {
	//		
	//	}

	void Awake() {
		// Get Components
		GameObject game = GameObject.Find("Game");
		gameManager = game.GetComponent<GameManager>();
	}

	public void Reset(Vector3 position) {
		// Reset stuff
		totalTime = Random.Range (40f, 80f);
		startX = position.x;
		endX = position.x - 1000f;
		// Set position
		gameObject.transform.position = position;
	}

	public void OnDestroy() {
		StopAllCoroutines ();
	}

	public void Restart() {
		StopAllCoroutines ();
		// Start
		StartCoroutine (MoveIt ());
	}

	void OnTriggerEnter2D(Collider2D other) {
		if (other.name == "Death") {
			StopAllCoroutines ();

			// Dispatch event
			if (OnCloudExpired != null)
				OnCloudExpired (this);
		}
	}

	IEnumerator MoveIt() {

		// Animate
		float t = 0.0f;
		while (t <= 1.0f) {
			if (!gameManager.GetIsPaused ()) {
				t += Time.deltaTime / totalTime;
				
				// Update position
				float newX = Mathf.Lerp (startX, endX, t);
				Vector3 newPosition = gameObject.transform.position;
				newPosition.x = newX;
				
				gameObject.transform.position = newPosition;
			}

			yield return null;
		}
	}
}
