﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour {

	public static event ExpireAction OnBulletExpired;
	public delegate void ExpireAction(Bullet bullet, int gunId);

	public GameObject graphic;
	public float lifeSpan = 5.0f;

	private GameManager gameManager;
	private Rigidbody2D rb2d;

	private int gunId;
	private Vector3 startPos;
	private float startTime;
	private float speed = 21.0f;

	void Awake() {
		// Get Components
		rb2d = GetComponent<Rigidbody2D> ();
		GameObject game = GameObject.Find("Game");
		gameManager = game.GetComponent<GameManager>();
	}

	public void Reset(Vector3 _position, float _lifeSpan, int _gunId) {
		// Set data
		startTime = Time.time;
		startPos = _position;
		lifeSpan = _lifeSpan;
 		gunId = _gunId;

		// Set start position
		gameObject.transform.localPosition = startPos;
	}

	public void StartLifespan() {
		// Start lifespan
		StartCoroutine (WaitForLifespan());
	}

	void Stop() {
		// Stop coroutines
		StopAllCoroutines ();

		// Dispatch expired event
		if (OnBulletExpired != null)
			OnBulletExpired(this, gunId);
	}

	public void FireLinearToPosition(Vector3 endPos, float _speed) {
		// Set rb properties
		rb2d.isKinematic = true;

		// Set data
		speed = _speed;

		// Start linear coroutine
		StartCoroutine (LinearToEnd (endPos));
	}

	public void FireForceWithVector(Vector2 v2) {
		// Set rb properties
		rb2d.isKinematic = false;

		// Add v2 as force
		rb2d.AddForce (v2);
	}

	void OnTriggerEnter2D(Collider2D other) {
		if (other.name == "Death") {
			// Stop
			Stop ();
		}
	}

	// Basic linear bullet travel
	IEnumerator LinearToEnd(Vector3 endPos) {
		float distTraveled = 0f;
		float totalDistance = Vector3.Distance (startPos, endPos);

		while (distTraveled < totalDistance) { 
			// Check game paused
			if (gameManager.GetIsPaused ())
				yield return null;

			// Calc current journey %
			distTraveled = (Time.time - startTime) * speed;
			float journey = distTraveled / totalDistance;

			// Lerp towards target
			Vector3 newPos = Vector3.Lerp (startPos, endPos, journey);

			// Set new position
			gameObject.transform.position = newPos;

			// Wait til next frame
			yield return null;
		}

		// Stop
		Stop ();
	}

	IEnumerator WaitForLifespan() {

		// Just wait
		yield return new WaitForSeconds(lifeSpan);

		// Stop
		Stop ();
	}
}
