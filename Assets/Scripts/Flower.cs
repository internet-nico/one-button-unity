﻿using UnityEngine;
using System.Collections;

public class Flower : MonoBehaviour {

	public static event ExpireAction OnFlowerExpired;
	public delegate void ExpireAction(Flower flower);

	private GameManager gameManager;

	// Use this for initialization
//	void Start () {
//		
//	}

	void Awake() {
		// Get Components
		GameObject game = GameObject.Find("Game");
		gameManager = game.GetComponent<GameManager>();
	}

	public void Reset(Vector3 position) {
		// Set position
		gameObject.transform.position = position;
	}

	public void OnDestroy() {
		StopAllCoroutines ();
	}

	public void Restart() {
		StopAllCoroutines ();
		// Start
		StartCoroutine (MoveIt ());
	}

	void OnTriggerEnter2D(Collider2D other) {
		if (other.name == "Death") {
			StopAllCoroutines ();

			// Dispatch event
			if (OnFlowerExpired != null)
				OnFlowerExpired (this);
		}
	}

	IEnumerator MoveIt() {

		while (true) {
			if (! gameManager.GetIsPaused ()) {
				Vector3 position = gameObject.transform.position;
				position.x -= 1;

				gameObject.transform.position = position;
			}

			yield return null;
		}
	}
}
