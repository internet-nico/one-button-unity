﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class GameManager : MonoBehaviour, IEventListener {

	private static string[] HURT_SOUNDS = new string[] {
		"3,.09,,.16,,.2573,.3,.4134,,-.6825,,,,,,,,,,,,,,,,1,,,,,,",
		"3,.09,,.15,,.2573,.3,.48,,-.88,,,,,,,,,,,,,,,,1,,,,,,"
	};

	private static string[] SHOOT_SOUNDS = new string[] {
		"1,.1,,.16,,.3018,.3,.25,.0358,-.36,,,,,,,,,,,.4262,-.4868,,.1371,-.1149,1,,,.1553,,,",
		"1,.1,,.16,,.3018,.3,.27,.0358,-.36,,,,,,,,,,,.4262,-.4868,,.1371,-.1149,1,,,.1553,,,",
		"1,.1,,.16,,.3018,.3,.29,.0358,-.36,,,,,,,,,,,.4262,-.4868,,.1371,-.1149,1,,,.1553,,,",
		"1,.1,,.16,,.3018,.3,.31,.0358,-.36,,,,,,,,,,,.4262,-.4868,,.1371,-.1149,1,,,.1553,,,"
	};

	public GameObject mainCamera;
	public GameObject player;
	public GameObject youDiedText;

	public string currentLevel = "Scene1";

	private SfxrSynth synth;
	private PlayerController playerController;
	private SmoothCamera2D smoothCamera;

	private bool isPaused = false;
	private bool isDead = false;
	private int score = 0;

	public bool GetIsPaused() {
		return isPaused;
	}
	public bool GetIsDead() {
		return isDead;
	}
	public float GetScore() {
		return score;
	}

	// Use this for initialization
	void Start () {
		// Set App preferences (if VSync off)
		Application.targetFrameRate = 60;
		Time.timeScale = 1f;

		synth = new SfxrSynth ();

		// Get components
//		synth = new SfxrSynth();
		smoothCamera = (SmoothCamera2D)mainCamera.GetComponent<SmoothCamera2D> ();
		playerController = player.GetComponent<PlayerController> ();

		Debug.Log ("GameManager.Start()");
		// Start level
		StartLevel ();
	}
	
	// Update is called once per frame
	void Update () {
		// Escape key
		if (Input.GetKeyDown (KeyCode.Escape)) {
			if (isPaused) {
				// Resume the game
				ResumeGame();
			} else {
				// Pause the game
				PauseGame();
			}
		}

		// Restart game if dead
		if (isDead && (Input.GetKeyDown (KeyCode.Space) || Input.GetMouseButton(0))) {
			RestartLevel ();
		}
	}

	void OnDestroy() {
		StopAllCoroutines ();
	}

	bool IEventListener.HandleEvent(IEvent evt)
	{
		switch (evt.GetName ()) {
		case "DeathEvent":
			PlayerDiedHandler ();
			break;
		case "BulletFiredEvent":
			BulletFiredHandler ();
			break;
		}

		return true;
	}

	void PlayerDiedHandler() {
		// Play sound
		var sound = HURT_SOUNDS[Random.Range(0, HURT_SOUNDS.Length-1)];
		synth.parameters.SetSettingsString(sound);
		synth.Play();

		// Shake camera
		CameraShake.Shake(0.21f, 4f);

		// Display text
		youDiedText.GetComponent<YouDiedText>().StartAnimations();

		// Pause game
		GameOver ();
	}

	void BulletFiredHandler() {
		// Play sound
		var sound = SHOOT_SOUNDS[Random.Range(0, SHOOT_SOUNDS.Length-1)];
		synth.parameters.SetSettingsString(sound);
		synth.Play();
	}

	void StartLevel() {
		playerController.Restart ();

		StartWarmup ();
	}

	void PauseGame() {
		Debug.Log ("Pause.");
		Time.timeScale = 0f;
		isPaused = true;
	}

	void ResumeGame() {
		Debug.Log ("Resume.");
		Time.timeScale = 1f;
		isPaused = false;
	}

	void GameOver() {
		Debug.Log ("Dead.");
		Time.timeScale = 0f;
		isPaused = true;
		isDead = true;

		// Remove listener
		EventManager.instance.DetachListener(this as IEventListener, "DeathEvent");
		EventManager.instance.DetachListener(this as IEventListener, "BulletFiredEvent");
	}

	void RestartLevel() {
		Debug.Log ("Reload scene.");
		SceneManager.LoadScene (currentLevel);
	}

	void StartWarmup() {
		Debug.Log ("Warmup.");

		isPaused = true;

		// Set new camera target
		smoothCamera.target = null;
		mainCamera.transform.position = new Vector3 (0f, 0f, -10f);

		// Wait a few sec, then begin
		Invoke ("BeginLevel", 2.25f);
	}

	void BeginLevel() {
		isDead = false;
		ResumeGame ();

		// Set new camera target
		smoothCamera.target = player.transform;

		// Listen for death
		EventManager.instance.AddListener(this as IEventListener, "DeathEvent");
		// Listen for shoot
		EventManager.instance.AddListener(this as IEventListener, "BulletFiredEvent");

		// Enable player
		playerController.EnablePhysics();

		// Start counting score
		StartCoroutine(KeepScore());
	}

	IEnumerator KeepScore() {
		while (true) {
			if (!isPaused) {
				score++;
//				Debug.Log ("score: " + score);

				yield return new WaitForSeconds (1f);
			}
		}
	}
}
