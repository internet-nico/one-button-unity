﻿using UnityEngine;
using System.Collections;

public class CloudSpawner : Spawner {

	public GameObject mainCamera;

	private Vector3 lastPosition;

	// Use this for initialization
	protected override void Start () {
		base.Start ();

		// Mark prefabs to spawn
		prefabNames = new string[]{ "Cloud1", "Cloud2", "Cloud3" };

		// Listen for events
		Cloud.OnCloudExpired += OnCloudExpiredHandler;

		// Start soon
		Invoke("Restart", Random.Range(5f, 15f));
	}

	protected override void RestartSpawnedObject(GameObject prefabGameObject) {
		// Get the script
		var cloud = prefabGameObject.GetComponent<Cloud> ();
		// Reset position
		Vector3 newPos = new Vector3(200f, Random.Range(40f, 70f));
		cloud.Reset (newPos);
		// Start moving 
		cloud.Restart ();

		// Change spawnRate for next time
		spawnRate = Random.Range(2f, 15f);
	}

	void OnCloudExpiredHandler(Cloud cloud) {
		// Despawn this gun
		base.DespawnGameObject (cloud.gameObject);
	}

	void LateUpdate() {
		Vector3 cameraPosition = mainCamera.transform.position;
		Vector3 currentPosition = transform.position;

		// Basic ass parallax
		if (lastPosition != null && lastPosition != cameraPosition) {
			float midPoint = (cameraPosition.y - currentPosition.y) * 0.5f;
			currentPosition.y = midPoint;

			transform.position = currentPosition;
		}

		lastPosition = cameraPosition;
	}
}
