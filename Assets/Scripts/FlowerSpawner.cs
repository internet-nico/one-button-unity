﻿using UnityEngine;
using System.Collections;

public class FlowerSpawner : Spawner {

	// Use this for initialization
	protected override void Start () {
		base.Start ();

		// Mark prefabs to spawn
		prefabNames = new string[]{ "Flower1", "Flower2", "Flower3" };

		// Listen for events
		Flower.OnFlowerExpired += OnFlowerExpiredHandler;

		// Start soon
		Invoke("Restart", Random.Range(1f, 5f));
	}

	protected override void RestartSpawnedObject(GameObject prefabGameObject) {
		// Get the script
		var flower = prefabGameObject.GetComponent<Flower> ();
		// Reset position
		flower.Reset (new Vector3(120f, -66f));
		// Start moving 
		flower.Restart ();

		// Change spawnRate for next time
		spawnRate = Random.Range(0.25f, 5f);
	}

	void OnFlowerExpiredHandler(Flower flower) {
		// Despawn this gun
		base.DespawnGameObject (flower.gameObject);
	}
}
