﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScoreText : MonoBehaviour {

	private GameManager gameManager;
	private Text theText;

	// Use this for initialization
	void Start () {
		// Get components
		GameObject game = GameObject.Find("Game");
		gameManager = game.GetComponent<GameManager>();

		theText = GetComponent<Text> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (gameManager != null)
			theText.text = gameManager.GetScore ().ToString ();
	}
}
